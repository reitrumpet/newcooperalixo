<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CooperaLixo</title>

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="css/css.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

</head>

<body>


<!-- Begin Form Auth -->


<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 animated bounceInDown">
            <h1 class="text-center login-title">Autentique-se e continue no <a href="index.php"
                                                                               class="bold">CooperaLixo</a></h1>
            <div class="account-wall">
                <img class="profile-img"
                     src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                     alt="">
                <form class="form-signin" method="post" action="#">
                    <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                    <input type="password" class="form-control" name="password" placeholder="Senha" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">
                        Entrar
                    </button>
                    <!--                    <label class="checkbox pull-left">-->
                    <!--                        <input type="checkbox" value="remember-me">-->
                    <!--                        Remember me-->
                    <!--                    </label>-->
                    <a href="contact.php" class="pull-right need-help">Precisa de ajuda? </a><span
                            class="clearfix"></span>
                </form>
            </div>
            <a href="register.php" class="text-center new-account">Crie uma conta </a>
            <p class="text-center text-muted">Copyright &copy; CooperaLixo - Rei Medeiros 2018</p>
        </div>
    </div>
</div>

<?php
include("openDatabase.php");
if (empty($_SESSION)) session_start();
if (count($_POST) > 0) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM `reg_user` WHERE `EMAIL`='$email' AND `PASSWORD`='$password'";
    $query = mysqli_query($strcon, $sql) or die(mysqli_error($strcon));

    while ($row = mysqli_fetch_array($query)) {
        $id = $row[0];
        $name = $row['2'];
        $cnpj = $row['CNPJ'];
    };



    if (isset($cnpj)) {

        if (mysqli_num_rows($query) >= 1) {
            $_SESSION['id'] = $id;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $password;
            header('Location: manage/index.php');
        } else {
            unset($_SESSION['id']);
            unset($_SESSION['name']);
            unset ($_SESSION['email']);
            unset ($_SESSION['password']);
            echo "<script> alert('Email ou senha errados.');</script>";
        }
    }
    if (empty($cnpj)) {
        if (mysqli_num_rows($query) >= 1) {
            $_SESSION['id'] = $id;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $password;
            header('Location: app/checker_device.php');
        } else {
            unset($_SESSION['id']);
            unset($_SESSION['name']);
            unset ($_SESSION['email']);
            unset ($_SESSION['password']);
            echo "<script> alert('Email ou senha errados.');</script>";
        }

    }


    mysqli_close($strcon);
}

?>


<!-- End Form Auth -->

</body>

</html>
