<!-- Footer -->
<?php

$year = date('Y');


?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <p class="copyright text-muted">Copyright &copy; CooperaLixo - Rei Medeiros <?php echo $year;?></p>
            </div>
        </div>
    </div>
</footer>

</body>

</html>
