<!-- SideBarNew -->

<?php include("header.php");?>
<nav>
    <div class="nav-wrapper container">
        <a href="index.php" class="brand-logo menu">CooperaLixo</a>
        <a href="#" data-activates="menu-mobile" class="button-collapse">
            <i class="material-icons">menu</i>
        </a>
        <ul class="right hide-on-med-and-down">
            <li><a class="menu" href="index.php">Painel</a></li>
            <li><a class="menu" href="delivery.php">Fa&ccedil;a uma Entrega</a></li>
            <li><a class="menu" href="historic.php">Hist&oacute;rico de Entregas</a></li>
            <li><a class="menu" href="ranking.php">Ranking</a></li>
            <li><a class="menu" href="edit.php">Editar Perfil</a></li>
            <li><a class="menu" href="logout.php">Logout</a></li>
        </ul>
        <ul class="side-nav menu" id="menu-mobile">
            <li><a class="menu" href="index.php">Painel</a></li>
            <li><a class="menu" href="delivery.php">Fa&ccedil;a uma Entrega</a></li>
            <li><a class="menu" href="historic.php">Hist&oacute;rico de Entregas</a></li>
            <li><a class="menu" href="ranking.php">Ranking</a></li>
            <li><a class="menu" href="edit.php">Editar Perfil</a></li>
            <li><a class="menu" href="logout.php">Logout</a></li>
        </ul>
    </div>
</nav>

<script>
    $(function () {
        $(".button-collapse").sideNav();
    });
</script>

<!-- SideBarNew -->


<!-- SideBarOld -->

