<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
    <!-- Início Conteúdo -->

    <div class="container top50">
        <div class="row border-groove padding20">
            <h2 class=""> Ranking </h2>
            <h4>Como est&aacute; a sua coloca&ccedil;&atilde;o ? </h4>
            <div class="top30">
                <?php
                include("../openDatabase.php");

                $total_reg = "10"; // número de registros por página

                if (isset($_GET['pagina'])) {
                    $pagina = $_GET['pagina'];
                } else {
                    $pagina = 1;
                }

                if (!$pagina) {
                    $pc = "1";
                } else {
                    $pc = $pagina;
                }

                $inicio = $pc - 1;
                $inicio = $inicio * $total_reg;


                $sql = "SELECT R.`NAME`, SUM(M.SCORE) AS SCORE FROM mat_delivery AS M
INNER JOIN reg_user AS R
ON M.ID_REG_USER = R.ID 
GROUP BY ID_REG_USER
ASC
HAVING SCORE > 0";


                $limite = mysqli_query($strcon, "$sql LIMIT $inicio, $total_reg") or die(mysqli_error($strcon));

                $todos = mysqli_query($strcon, "$sql") or die(mysqli_error($strcon));

                $tr = mysqli_num_rows($todos); // verifica o número total de registros
                $tp = $tr / $total_reg; // verifica o número total de páginas

                echo "<table class='table table-bordered table-responsive'>";
                echo "<tr class='height50'>";
                echo "<td class='negrito'>Usu&aacute;rio</td>";
                echo "<td class='negrito'>Pontua&ccedil;&atilde;o</td>";
                echo "</tr>";

                while ($row = mysqli_fetch_array($limite)) {
                    echo "<tr class='height50'>";
                    echo "<td>" . $row[0] . "</td>";
                    echo "<td>" . $row[1] . "</td>";
                    echo "</tr>";
                }

                // agora vamos criar os botões "Anterior e próximo"
                $anterior = $pc -1;
                $proximo = $pc +1;
                if ($pc>1) {
                    echo " <a href='?pagina=$anterior'><- Anterior</a> ";
                }
                echo "|";
                if ($pc<$tp) {
                    echo " <a href='?pagina=$proximo'>Próxima -></a>";
                }

                //                $query = mysqli_query($strcon, $sql) or die(mysqli_error($strcon));
                //
                //                echo "<table class='table table-bordered table-responsive'>";
                //                echo "<tr class='height50'>";
                //                echo "<td class='negrito'>Usu&aacute;rio</td>";
                //                echo "<td class='negrito'>Pontua&ccedil;&atilde;o</td>";
                //                echo "</tr>";
                //                while ($row = mysqli_fetch_array($query)) {
//                                    echo "<tr class='height50'>";
//                                    echo "<td>" . $row[0] . "</td>";
//                                    echo "<td>" . $row[1] . "</td>";
//                                    echo "</tr>";
                //                }
                mysqli_close($strcon);
                echo "</tr>";
                echo "</table>";

                ?>
            </div>
        </div>
    </div>


    <!-- Fim Conteúdo -->
<?php include("footer.php"); ?>