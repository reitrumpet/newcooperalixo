<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
    <!-- Início Conteúdo -->

    <div class="container top50">
        <div class="row border-groove padding20">
            <h2 class=""> Fa&ccedil;a uma Entrega </h2>

<!--            --><?php //echo $_GET['id']; ?>

            <!-- Begin Form -->

            <div class="site-heading">

                <form method="post" action="">
                    <h4 class="login padding10">Registre-se</h4>
<!--                    <div class="bottom10">-->
<!--                        <div class="floatLeft">Descri&ccedil;&atilde;o*</div>-->
<!--                        <input size="60" maxlength="255" class="form-control" placeholder="Descri&ccedil;&atilde;o"-->
<!--                               name="description"-->
<!--                               type="text" required/>-->
<!--                    </div>-->
                    <div class="bottom10">
                        <div class="floatLeft">Quantidade</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Quantidade" name="qtd"
                               type="number" min="1" max="1000" required/>
                    </div>
                    <input class="login padding10 cursorPointer" type="submit" value="Salvar">
                </form>

                <?php

                if (count($_POST) > 0) {
                    $situation = "Pendente";
                    $qtd = $_POST['qtd'];
                    $idType = $_GET['id'];
                    $idUser = $_SESSION['id'];
                    $date = date('Y-m-d');
                    $measure = "KG";


                    if ($idType == 1) {
                        $score = 1 * $qtd;
                    }
                    if ($idType == 2) {
                        $score = 3 * $qtd;
                    }
                    if ($idType == 3) {
                        $score = 5 * $qtd;
                    }
                    if ($idType == 4) {
                        $score = 5 * $qtd;
                    }
                    if ($idType == 5) {
                        $score = 1 * $qtd;
                    }

                    include("../openDatabase.php");
                    $sql = "INSERT INTO `mat_delivery`
(`ID_TYPE`, `QTD`, `MEASURE`, `ID_REG_USER`, `DATE`, `SCORE`, `SITUATION`) 
VALUES ($idType,$qtd,'$measure',$idUser,'$date',$score,'$situation')";

                    if ($query = mysqli_query($strcon, $sql) or die(mysqli_error($strcon))) {
                        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=delivery.php'>";
                        echo "<script> alert('Entrega realizada com sucesso, aguarde ser validado para ter a pontuação liberada.');</script>";
                        mysqli_close($strcon);
                    } else {
                        echo "<script> alert('Erro ao realizar a entrega.');</script>";
                    }
                }
                ?>
            </div>

            <!-- End Form -->

        </div>
    </div>

    <!-- Fim Conteúdo -->
<?php include("footer.php"); ?>