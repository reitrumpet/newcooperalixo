<?php

include "header.php";

?>
<header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Contato</h1>
                    <span class="subheading">Tem alguma pergunta? Eu tenho a resposta :).</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <p>Quer entrar em contato ? Preencha o formul&aacute;rio para me enviar uma mensagem e eu entrarei em
                contato com voc&ecirc; o mais breve poss&iacute;vel!</p>
            <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
            <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
            <!-- To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
            <form name="sentMessage" id="contactForm" novalidate action="mail/contact_me.php">
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Nome</label>
                        <input type="text" class="form-control" placeholder="Nome" id="name" required
                               data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Email</label>
                        <input type="email" class="form-control" placeholder="E-mail" id="email" required
                               data-validation-required-message="Please enter your email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                        <label>Telefone</label>
                        <input type="tel" class="form-control" placeholder="Telefone" id="phone" required
                               data-validation-required-message="Please enter your phone number.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Mensagem</label>
                        <textarea rows="5" class="form-control" placeholder="Mensagem" id="message" required
                                  data-validation-required-message="Please enter a message."></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" id="sendMessageButton">Enviar</button>
                </div>
            </form>
            <?php
            if (isset($_GET["emailSend"])){

                $emailSend = $_GET["emailSend"];
                if ($emailSend == "no"){
                    echo "<script> alert('A mensagem não foi enviada.');</script>";
                }elseif($emailSend == "yes"){
                    echo "<script> alert('Mensagem enviada com sucesso.');</script>";
                }

            }


            ?>
        </div>
    </div>
</div>

<hr>

<?php

include "footer.php";

?>
